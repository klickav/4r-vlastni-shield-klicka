# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **čistý čas** |cca 7 hodin |
| jak se mi to podařilo rozplánovat | docela dobře|
| design zapojení | https://gitlab.spseplzen.cz/klickav/4r-vlastni-shield-klicka/-/blob/main/dokumentace/schema/zapojeni-shield.jpg|
| proč jsem zvolil tento design | přišel mi nejlepší |
| zapojení | https://gitlab.spseplzen.cz/klickav/4r-vlastni-shield-klicka/-/blob/main/dokumentace/fotky/370718049_1267914503776351_950706471445152945_n.jpg |
| z jakých součástí se zapojení skládá | fotorezistor, dva rezistory, enkoder, teplomer, ledpasek, dvě ledky, dratky |
| realizace | https://gitlab.spseplzen.cz/klickav/4r-vlastni-shield-klicka/-/blob/main/dokumentace/fotky/385369819_170662726089387_3643153589893248138_n.jpg |
| nápad, v jakém produktu vše propojit dohromady| teplotní čidlo do místnosti  |
| co se mi povedlo | zapojní |
| co se mi nepovedlo/příště bych udělal/a jinak | rozložení komponent |
| zhodnocení celé tvorby | vcelku se mi to povedlo |
